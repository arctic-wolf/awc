fn main() {
    println!(
        "{} - Arctic Fox Console {}",
        env!("CARGO_BIN_NAME"),
        env!("CARGO_PKG_VERSION")
    );
}
